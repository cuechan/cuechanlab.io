#!/bin/bash

root=$1

echo "<h1>Files</h1>"

function create_list {
	echo "<ul>"

	for filepath in `find "$1" -maxdepth 1 -mindepth 1 -type d | sort`; do
		pathname=`basename "$filepath"`
		echo "<li> $pathname"
		create_list $filepath
		echo "</li>"
	done

	for filepath in `find "$1" -maxdepth 1 -mindepth 1 -type f | sort`; do
		pathname=`basename "$filepath"`
		echo "<li><a href=\"$filepath\">$pathname</a></li>"
	done

	echo "</ul>"
}


create_list $root
